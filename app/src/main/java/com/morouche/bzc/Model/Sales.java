package com.morouche.bzc.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.morouche.bzc.ListAdapter.CustomListAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mehdi on 28/02/2017.
 */
public class Sales {

    private String name, store, description, duree, imagePath;
    private Bitmap image;
    private CustomListAdapter adapter;
    private Context context;
    private Boolean fileExist = false;

    public Sales(String name, String store, String description, String duree, String image, CustomListAdapter adapter, Context context) {
        this.adapter = adapter;
        this.name = name;
        this.store = store;
        this.description = description;
        this.duree = duree;
        this.context = context;
        this.imagePath = image;

        if(image.length()>4) {
            List<String> files = getImageList(new File(context.getCacheDir() + File.separator));

            //On regarde si on ne possède pas déjà l'image afin d'éviter de la télécharger inutilement.
            if(files.size() > 0) {
                for(String fileName : files) {
                    if(fileName.equals(name+".jpg") || fileName.equals(name+".png")) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(context.getCacheDir() + File.separator + fileName );
                        this.image = myBitmap;
                        myBitmap = null;
                        fileExist = true;
                    }
                }
                if(!fileExist) {
                    new DownloadImageTask(false).execute(image);
                }
            }
            else {
                new DownloadImageTask(false).execute(image);
            }
        }
        fileExist = false;

    }

    public String getName() {
        return name;
    }

    public String getStore() {
        return store;
    }

    public String getDescription() {
        return description;
    }

    public String getDuree() {
        return duree;
    }

    public Bitmap getImage() {
        return image;
    }

    //Permet de lister les images déjà présentent dans le dossier cache de l'application
    private List<String> getImageList(File parentDir) {

        ArrayList<String> inFiles = new ArrayList<String>();
        String[] fileNames = parentDir.list();

        for (String fileName : fileNames) {
            if (fileName.toLowerCase().endsWith(".jpg") || fileName.toLowerCase().endsWith(".png")) {
                inFiles.add(fileName);
            }
        }

        return inFiles;
    }

    //AsynTask qui permet de télécharger les images en asynchrone et de mettre à jour la listView
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        Boolean isImageProfil;

        public DownloadImageTask(Boolean imageProfil){
            isImageProfil = imageProfil;
        }

        protected Bitmap doInBackground(String... urls) {
            String urlImage = urls[0];
            Bitmap bm = null;
            try {
                InputStream in = new java.net.URL(urlImage).openStream();
                bm = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bm;
        }

        protected void onPostExecute(Bitmap result) {
            try {

                File cacheFile = new File(context.getCacheDir() + File.separator, name+".jpg");

                FileOutputStream fos = new FileOutputStream(cacheFile);
                result.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();

                Bitmap myBitmap = BitmapFactory.decodeFile(cacheFile.getAbsolutePath());

                image = myBitmap;

                adapter.notifyDataSetChanged();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}