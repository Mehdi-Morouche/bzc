package com.morouche.bzc.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.morouche.bzc.ListAdapter.CustomListAdapter;
import com.morouche.bzc.R;
import com.morouche.bzc.Model.Sales;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Mehdi on 28/02/2017.
 */
public class MyListFragment extends ListFragment implements SwipeRefreshLayout.OnRefreshListener {

    private CustomListAdapter adapter;
    private ArrayList<Sales> salesList = new ArrayList<Sales>();
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.liste, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(Color.parseColor("#FFB74D"));

        adapter = new CustomListAdapter(this, salesList);

        setListAdapter(adapter);

        if(salesList.isEmpty())
            onRefresh();

        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        FragmentManager fragmentManager = this.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        MyDetailFragment detail = new MyDetailFragment();
        detail.setItem(salesList.get(position));

        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(R.id.linear, detail, "DetailFragment");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);

        RequestQueue queue = Volley.newRequestQueue(this.getContext());
        String url ="https://api.gilt.com/v1/sales/active.json?apikey=e8a850cdcf1aa4fc2b06c5c3118b211142dcba68b80450d9646de8e177184c7a";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jso = new JSONObject(response);
                            JSONArray list = jso.getJSONArray("sales");

                            //On créer des objets Sales que l'on ajoute à notre liste.
                            for(int i=0; i<list.length(); i++) {
                                JSONObject jsobj = list.getJSONObject(i);

                                Sales s = new Sales(jsobj.get("name").toString(), jsobj.get("store").toString(), jsobj.get("description").toString(), jsobj.get("ends").toString(), jsobj.getJSONObject("image_urls").getJSONArray("315x295").getJSONObject(0).get("url").toString(), adapter, getContext());
                                salesList.add(s);
                            }
                            adapter.notifyDataSetChanged();
                            swipeRefreshLayout.setRefreshing(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        queue.add(stringRequest);
    }
}