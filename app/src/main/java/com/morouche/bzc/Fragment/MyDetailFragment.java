package com.morouche.bzc.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.morouche.bzc.R;
import com.morouche.bzc.Model.Sales;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mehdi on 28/02/2017.
 */
public class MyDetailFragment extends Fragment {
    private Sales _sale;
    private TextView titre, store, description, duree;
    private ImageView image;
    private ScrollView scrollDetail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.detail, container, false);

        scrollDetail = (ScrollView) view.findViewById(R.id.scrollDetail);
        titre = (TextView) view.findViewById(R.id.name);
        store = (TextView) view.findViewById(R.id.store);
        description = (TextView) view.findViewById(R.id.description);
        duree = (TextView) view.findViewById(R.id.duree);
        image = (ImageView) view.findViewById(R.id.image);

        titre.setText(_sale.getName());
        store.setText(_sale.getStore());
        description.setText(_sale.getDescription());
        image.setImageBitmap(_sale.getImage());

        duree.setText(countdownSale(_sale.getDuree()));

        return view;
    }

    public void setItem(Sales sale) {
        _sale = sale;
    }

    //Méthode qui permet de calculer le temps restant entre 2 dates
    public String countdownSale(String dateFin) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        try {

            Date date2 = simpleDateFormat.parse(dateFin);
            Date date1 = Calendar.getInstance().getTime();

            long different = date2.getTime() - date1.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;


            if(elapsedDays > 1)
                return "Reste " + elapsedDays + " jours";
            else if(elapsedDays == 1)
                return "Reste " + elapsedDays + " jour";
            else if(elapsedHours > 1)
                return "Reste " + elapsedHours + " heures";
            else if(elapsedHours == 1)
                return "Reste " + elapsedHours + "heure";
            else if(elapsedMinutes > 1)
                return "Reste " + elapsedMinutes + " minutes";
            else if(elapsedMinutes == 1)
                return "Reste " + elapsedMinutes + " minute";
            else
                return "Vente finie";

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }
}