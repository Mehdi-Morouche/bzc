package com.morouche.bzc.ListAdapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.morouche.bzc.R;
import com.morouche.bzc.Model.Sales;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Mehdi on 28/02/2017.
 */
public class CustomListAdapter extends BaseAdapter {
    private Fragment activity;
    private LayoutInflater inflater;
    private List<Sales> saleItems;

    public CustomListAdapter(ListFragment activity, List<Sales> saleItems) {
        this.activity = activity;
        this.saleItems = saleItems;
    }

    @Override
    public int getCount() {
        return saleItems.size();
    }

    @Override
    public Object getItem(int location) {
        return saleItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row, null);

        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        TextView title = (TextView) convertView.findViewById(R.id.name);
        TextView duree = (TextView) convertView.findViewById(R.id.duree);

        image.setImageBitmap(saleItems.get(position).getImage());
        title.setText(saleItems.get(position).getName());
        duree.setText(countdownSale(saleItems.get(position).getDuree()));

        return convertView;
    }

    //Méthode qui permet de calculer le temps restant entre 2 dates
    public String countdownSale(String dateFin) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        try {

            Date date2 = simpleDateFormat.parse(dateFin);
            Date date1 = Calendar.getInstance().getTime();

            long different = date2.getTime() - date1.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;


            if(elapsedDays > 1)
                return "Reste " + elapsedDays + " jours";
            else if(elapsedDays == 1)
                return "Reste " + elapsedDays + " jour";
            else if(elapsedHours > 1)
                return "Reste " + elapsedHours + " heures";
            else if(elapsedHours == 1)
                return "Reste " + elapsedHours + "heure";
            else if(elapsedMinutes > 1)
                return "Reste " + elapsedMinutes + " minutes";
            else if(elapsedMinutes == 1)
                return "Reste " + elapsedMinutes + " minute";
            else
                return "Vente finie";

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }
}
